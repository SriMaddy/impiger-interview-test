package com.example.impigerinterview.view

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.impigerinterview.R
import com.example.impigerinterview.model.User

class UserDetailActivity : AppCompatActivity() {

    lateinit var selectedUser: User

    private lateinit var userImage: ImageView
    private lateinit var userName: TextView
    private lateinit var userRole: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_detail)

        userImage = findViewById(R.id.userImage)
        userName = findViewById(R.id.userName)
        userRole = findViewById(R.id.userRole)

        selectedUser = intent.extras!!["selectedUser"] as User
        setValues()
    }

    private fun setValues() {
        userName.text = selectedUser.name
        userRole.text = selectedUser.role

        Glide.with(this.applicationContext)
            .load(selectedUser.imageUrl)
            .centerCrop()
            .into(userImage)
    }
}