package com.example.impigerinterview.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.example.impigerinterview.R
import com.example.impigerinterview.adapter.UserAdapter
import com.example.impigerinterview.model.User
import com.example.impigerinterview.viewmodel.UserViewModel
import com.example.impigerinterview.viewmodel.UserViewModelFactory

class UserActivity : AppCompatActivity() {

    lateinit var userAdapter: UserAdapter
    lateinit var userViewModel: UserViewModel
    private var userViewModelFactory: UserViewModelFactory = UserViewModelFactory()

    lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)

        recyclerView = findViewById(R.id.recyclerView)
        userViewModel = ViewModelProvider(this, userViewModelFactory).get(UserViewModel::class.java)

        // add user data
//        var users: List<User> = listOf(
//            User(name = "Sridhar Android", role = "Android Developer", imageUrl = "https://logos-download.com/wp-content/uploads/2016/05/Android_logo_wordmark_logotype-700x162.png"),
//            User(name = "Sridhar iOS", role = "iOS Developer", imageUrl = "https://www.cbronline.com/wp-content/uploads/2016/05/iOS.jpg"),
//            User(name = "Sridhar Flutter", role = "Flutter Developer", imageUrl = "https://itcraftapps.com/wp-content/uploads/2019/03/Flutter-Cover.png"),
//            User(name = "Sridhar Web", role = "Web Developer", imageUrl = "https://cdn57.androidauthority.net/wp-content/uploads/2018/08/learn-web-development-920x470.jpg"),
//            User(name = "Sridhar Full Stack", role = "Full Stack Developer", imageUrl = "https://radicalhub.com/wp-content/uploads/2018/07/javascript.jpg")
//        )

//        userViewModel.addUserData(this.applicationContext, users)

        // clear all user data
//        userViewModel.clearAllUserData(this.applicationContext)
//        userViewModel.deleteResult.observe(this, Observer {
//            if (it) {
//                Log.i("UserTableEntries", "Deleted")
//            }
//        })

        // observer to observe user data
        userViewModel.getUserData(this.applicationContext)
        userViewModel.users.observe(this, Observer {
            it?.forEach { user ->
                Log.i("UserTableEntries", user.toString())
            }
            userAdapter = UserAdapter(it, this.applicationContext) {
                moveToDetailPage(it)
            }
            recyclerView.adapter = userAdapter
        })

    }

    private fun moveToDetailPage(user: User) {
        val intent = Intent(this, UserDetailActivity::class.java)
        intent.putExtra("selectedUser", user)
        startActivity(intent)
    }
}