package com.example.impigerinterview.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.impigerinterview.R
import com.example.impigerinterview.model.User
import de.hdodenhof.circleimageview.CircleImageView

class UserAdapter(private val users: MutableList<User>, private val context: Context, private val onClick: (User) -> Unit) : RecyclerView.Adapter<UserAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var userImage: CircleImageView = itemView.findViewById(R.id.userImage)
        var userName: TextView = itemView.findViewById(R.id.userName)
        var userRole: TextView = itemView.findViewById(R.id.userRole)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.user_list_item, parent, false))
    }

    override fun getItemCount() = users.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = users[position]

        holder.userName.text = user.name
        holder.userRole.text = user.role

        Log.d("ImageUrl", user.imageUrl)

        Glide.with(context)
            .load(user.imageUrl)
            .into(holder.userImage)

        holder.itemView.setOnClickListener {
            onClick(user)
        }
    }
}