package com.example.impigerinterview.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.impigerinterview.model.User
import com.example.impigerinterview.model.dao.UserDao
import com.example.impigerinterview.model.database.DatabaseClient
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class UserViewModel : ViewModel(), CoroutineScope {

    private var coroutineJob: Job = Job()
    override val coroutineContext: CoroutineContext
        get() = coroutineJob + Dispatchers.IO

    var users = MutableLiveData<MutableList<User>>()
    var deleteResult = MutableLiveData<Boolean>()

    override fun onCleared() {
        super.onCleared()
        coroutineJob.cancel()
    }

    fun getUserData(context: Context) {
        launch {
            try {
                // business logic to fetch data from Room DB
                val userDao: UserDao = DatabaseClient.getInstance(context).getDatabase().getUserDao()
                val usersList = userDao.getAllUsers()

                // set to live data
                withContext(Dispatchers.Main) {
                    users.value = usersList.toMutableList()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun clearAllUserData(context: Context) {
        launch {
            try {
                val userDao: UserDao = DatabaseClient.getInstance(context).getDatabase().getUserDao()
                userDao.clearAllUserData()
//                val rowCount = userDao.getCount()

                withContext(Dispatchers.Main) {
                    deleteResult.value = true
                }
            } catch (e: Exception) {
                e.printStackTrace()
                withContext(Dispatchers.Main) {
                    deleteResult.value = false
                }
            }
        }
    }

    fun addUserData(context: Context, users: List<User>) {
        launch {
            try {
                users?.forEach {
                    val userDao: UserDao = DatabaseClient.getInstance(context).getDatabase().getUserDao()
                    userDao.insertUser(it)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}