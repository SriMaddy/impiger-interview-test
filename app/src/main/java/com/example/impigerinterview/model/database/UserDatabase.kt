package com.example.impigerinterview.model.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.impigerinterview.model.User
import com.example.impigerinterview.model.dao.UserDao

@Database(entities = [User::class], version = 1)
abstract class UserDatabase : RoomDatabase() {

    abstract fun getUserDao(): UserDao
}