package com.example.impigerinterview.model.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.impigerinterview.model.User

@Dao
interface UserDao {

    @Query("SELECT * FROM User")
    fun getAllUsers(): List<User>

    @Query("DELETE from User")
    fun clearAllUserData()

    @Insert
    fun insertUser(user: User)

//    @Query("SELECT COUNT(*) FROM User")
//    fun getCount(): Int
}