package com.example.impigerinterview.model.database

import android.content.Context
import androidx.room.Room

class DatabaseClient(private val context: Context) {

    private var userDatabase: UserDatabase = Room.databaseBuilder(context, UserDatabase::class.java, "UserDB").build()

    companion object {
        private var databaseClient: DatabaseClient? = null
        fun getInstance(context: Context): DatabaseClient {
            if (databaseClient == null) {
                databaseClient = DatabaseClient(context)
            }
            return databaseClient!!
        }
    }

    fun getDatabase(): UserDatabase = userDatabase
}